function show_hide_password(target){
	var input = document.getElementById('password-input');
	if (input.getAttribute('type') == 'password') {
		target.classList.add('view');
		input.setAttribute('type', 'text');
	} else {
		target.classList.remove('view');
		input.setAttribute('type', 'password');
	}
	return false;
}
function initMap() {
    var coordinates = {lat: 55.04023390281978, lng: 82.89655252040886},
    popupContent = '<p class="content">Что угодно</p>';
    markerImage = '/svg/icons/marker.png';

    map = new google.maps.Map(document.getElementById('map'), {
    	center: {lat: coordinates.lat, lng: coordinates.lng},
		zoom:19,
		mapId:'89431fb247231021'
    });

    marker = new google.maps.Marker({
		position: coordinates,
        map: map,
        icon: markerImage
   });

    infowindow = new google.maps.InfoWindow({
        content: popupContent
	});
}